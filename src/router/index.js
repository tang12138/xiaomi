import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home'
import Index from '../pages/home/components/Index'
// import Detail from '../pages/detail/Detail'
// import Login from '../pages/login/Login'
// import ShoppingCart from '../pages/shopping-cart/ShoppingCart'
// import Order from '../pages/order/Order'
// import OrderList from '../pages/order/components/OrderList'
// import OrderAliPay from '../pages/order/components/OrderAliPay'
// import OrderConfirm from '../pages/order/components/OrderConfirm'
// import OrderPay from '../pages/order/components/OrderPay'
// import Product from '@/pages/product/Product'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/index',
    component: Home,
    children: [
      {
        path: 'index',
        name: 'index',
        component: Index,
      },
      {
        path: 'Product/:id',
        name: 'Product',
        component: () => import('../pages/product/Product.vue'),
      },
      {
        path: 'detail/:id',
        name: 'detail',
        component: () => import('../pages/detail/Detail.vue'),
      },
    ],
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../pages/login/Login.vue'),
  },
  {
    path: '/shoppingCart',
    name: 'shoppingCart',
    component: () => import('../pages/shopping-cart/ShoppingCart.vue'),
  },
  {
    path: '/order',
    name: 'order',
    component: () => import('../pages/order/Order.vue'),
    children: [
      {
        path: 'list',
        name: 'order-list',
        component: () => import('../pages/order/components/OrderList.vue'),
      },
      {
        path: 'confirm',
        name: 'order-confirm',
        component: () => import('../pages/order/components/OrderConfirm.vue'),
      },
      {
        path: 'pay',
        name: 'order-pay',
        component: () => import('../pages/order/components/OrderPay.vue'),
      },
      {
        path: 'alipay',
        name: 'order-alipay',
        component: () => import('../pages/order/components/OrderAliPay.vue'),
      },
    ],
  },
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})


export default router
