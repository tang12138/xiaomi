import axios from 'axios';
import qs from 'qs';

//随着环境变化更改接口地址
switch (process.env.NODE_ENV) {
  case 'production':
    axios.defaults.baseURL = 'http://127.0.0.1:3000'
    break
  case 'test':
    axios.defaults.baseURL = 'http://192.168.0.1:3000'
    break
  default:
    axios.defaults.baseURL = 'http://192.168.0.1:3000'//根据实际接口地址填写
}

/*
 * 设置超时时间和跨域是否允许携带凭证
 */
axios.defaults.timeout = 10000;
axios.defaults.withCredentials = true

/*
设置请求传递数据的格式（看服务器需要什么格式）
x-www-form-urlencoded
*/
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.transformRequest = data => qs.stringify(data)

/*
设置请求拦截器
客户端发送请求---[请求拦截器]---服务器
token校验，接收服务器返回的token，存储到本地存储中，每一次向服务器发送请求，要把token带上
*/
axios.interceptors.request.use(config => {
  // 携带上token
  let token = localStorage.getItem('token')
  token && (config.headers.Authorization = token)
  return config;
}, error => {
  // Do something with request error
  return Promise.reject(error);
});

/*
 *响应拦截器
  服务器返回信息---响应拦截器---客户端获取到信息
 */
axios.defaults.validateStatus = status => {
  // 自定义响应成功的状态码
  return /^(2|3)\d{2}$/.test(status)
}

axios.interceptors.request.use(response => {
  let res = response.data
  if (res.status == 0) {//如果请求成功，则直接将请求到的主体返回，就不用res.data.data获取了
    return res.data
  }
  return
}, error => {
  let { response } = error
  if (response) {
    //如果response存在，至少服务器返回结果了
  } else {
    //服务器连结果都没返回
  }
  return Promise.reject(error);
}).catch(error => {
  if (!window.navigator.onLine) {
    //断开网络了，可以让其跳到断网页面
  }
});