import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'
import { Message, MessageBox } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'iconfont/iconfont.css'
import 'scss/reset.scss'
import VueCookie from 'vue-cookie'
import VueLazyLoad from 'vue-lazyload'

axios.defaults.baseURL = '/api'
axios.defaults.timeout = 8000
//接口错误拦截
axios.interceptors.response.use(function (response) {
  let path = location.pathname
  let res = response.data
  console.log(response);
  console.log(res);
  if (res.status == 0) {
    return res.data
  } else if (res.status == 10) {
    if (path != '/index' && path != '/login' || path == '/index') {
      MessageBox.confirm('是否跳转到登录页面（为方便体验会自动填写帐号密码）', '提示', {
        type: 'warning'
      }).then(() => {
        window.location.href = '/login'
      })
      return Promise.reject(res)
    }
  } else {
    return Promise.reject(res)
  }
})

Vue.use(VueAxios, axios)
Vue.use(VueCookie)
Vue.use(VueLazyLoad, {
  loading: require('imgs/loading-svg/loading-bubbles.svg'),
})
Vue.prototype.$message = Message
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
