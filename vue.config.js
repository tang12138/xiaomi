// const path = require('path') // 引入path模块
// function resolve(dir) {
//   return path.join(__dirname, dir) // path.join(__dirname)设置绝对路径
// }
module.exports = {
  // publicPath: process.env.NODE_ENV === 'production'
  //   ? './'
  //   : '/',
  css: {
    loaderOptions: {
      sass: {
        //sass-loader8.0.2版本使用prependData，最新版要使用additionalData
        prependData: `
        @import "@/assets/scss/mixin.scss";
        @import "@/assets/scss/config.scss";
      `,
      },
    },
  },
  productionSourceMap: false,
  chainWebpack: (config) => {
    config.plugins.delete('prefetch')
  },
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  indexPath: 'index.html',
  // chainWebpack: (config) => {
  //   config.resolve.alias
  //     .set('@', resolve('./src'))
  //     .set('iconfont', resolve('./src/assets/iconfont/'))
  //     .set('common', resolve('./src/common/'))
  //     .set('imgs', resolve('./src/assets/imgs/'))
  //     .set('pages', resolve('./src/pages/'))
  //     .set('scss', resolve('./src/assets/scss/'))
  //   config.plugins.delete('prefetch')
  //   // set第一个参数：设置的别名，第二个参数：设置的路径
  // },
  configureWebpack: {
    resolve: {
      alias: {
        // "@":"./src",
        iconfont: '@/assets/iconfont/',
        common: '@/common/',
        imgs: '@/assets/imgs/',
        pages: '@/pages/',
        scss: '@/assets/scss/',
      },
    },
    devtool: 'source-map',
  },
  devServer: {
    host: 'localhost',
    port: 8080,
    proxy: {
      '/api': {
        target: 'http://mall-pre.springboot.cn',
        changeOrigin: true,
        pathRewrite: {
          '/api': '',
        },
      },
    },
  },
}
